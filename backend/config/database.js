const path = require("path");

module.exports = ({ env }) => {
  //
  const client = env("DATABASE_CLIENT", "mysql"); // tạo kết nối  đến mysql

  const connections = {
    mysql: {
      connection: {
        // thông tin kết nối cơ sở dữ liệu
        connectionString: env("DATABASE_URL"), // sử dụng  để kết nối CSDL
        host: env("DATABASE_HOST", "localhost"), //127.0.0.1
        port: env.int("DATABASE_PORT", 3306),
        database: env("DATABASE_NAME", "strapi"), //backend
        user: env("DATABASE_USERNAME", "strapi"),
        password: env("DATABASE_PASSWORD", "strapi"),
        //ssl bảo mật false ,Đối với kết nối cơ sở dữ liệu SSL.Sử dụng một đối tượng để truyền tệp chứng chỉ dưới dạng chuỗi.
        ssl: env.bool("DATABASE_SSL", false) && {
          key: env("DATABASE_SSL_KEY", undefined),
          cert: env("DATABASE_SSL_CERT", undefined),
          ca: env("DATABASE_SSL_CA", undefined),
          capath: env("DATABASE_SSL_CAPATH", undefined),
          cipher: env("DATABASE_SSL_CIPHER", undefined),
          rejectUnauthorized: env.bool(
            "DATABASE_SSL_REJECT_UNAUTHORIZED",
            true
          ),
        },
      },
      pool: {
        // tập hợp các tài nguyên ( ví dụ kết nối csdl)
        min: env.int("DATABASE_POOL_MIN", 2), //	Minimum number of database connections to keepalive
        max: env.int("DATABASE_POOL_MAX", 10), // Số lượng kết nối cơ sở dữ liệu tối đa để keepalive

        // Việc sử dụng pool có thể cải thiện hiệu suất và tốc độ xử lý
        //của ứng dụng bằng cách giảm thiểu số lượng tài nguyên cần phải tạo ra và xóa bỏ liên tục.
      },
    },
    mysql2: {
      connection: {
        host: env("DATABASE_HOST", "localhost"),
        port: env.int("DATABASE_PORT", 3306),
        database: env("DATABASE_NAME", "strapi"),
        user: env("DATABASE_USERNAME", "strapi"),
        password: env("DATABASE_PASSWORD", "strapi"),
        ssl: env.bool("DATABASE_SSL", false) && {
          key: env("DATABASE_SSL_KEY", undefined),
          cert: env("DATABASE_SSL_CERT", undefined),
          ca: env("DATABASE_SSL_CA", undefined),
          capath: env("DATABASE_SSL_CAPATH", undefined),
          cipher: env("DATABASE_SSL_CIPHER", undefined),
          rejectUnauthorized: env.bool(
            "DATABASE_SSL_REJECT_UNAUTHORIZED",
            true
          ),
        },
      },
      pool: {
        min: env.int("DATABASE_POOL_MIN", 2),
        max: env.int("DATABASE_POOL_MAX", 10),
      },
    },
    postgres: {
      connection: {
        connectionString: env("DATABASE_URL"),
        host: env("DATABASE_HOST", "localhost"),
        port: env.int("DATABASE_PORT", 5432),
        database: env("DATABASE_NAME", "strapi"),
        user: env("DATABASE_USERNAME", "strapi"),
        password: env("DATABASE_PASSWORD", "strapi"),
        ssl: env.bool("DATABASE_SSL", false) && {
          key: env("DATABASE_SSL_KEY", undefined),
          cert: env("DATABASE_SSL_CERT", undefined),
          ca: env("DATABASE_SSL_CA", undefined),
          capath: env("DATABASE_SSL_CAPATH", undefined),
          cipher: env("DATABASE_SSL_CIPHER", undefined),
          rejectUnauthorized: env.bool(
            "DATABASE_SSL_REJECT_UNAUTHORIZED",
            true
          ),
        },
        schema: env("DATABASE_SCHEMA", "public"),
      },
      pool: {
        min: env.int("DATABASE_POOL_MIN", 2),
        max: env.int("DATABASE_POOL_MAX", 10),
      },
    },
    sqlite: {
      connection: {
        filename: path.join(
          __dirname,
          "..",
          env("DATABASE_FILENAME", "data.db")
        ),
      },
      useNullAsDefault: true, //Sử dụng NULL làm giá trị mặc định
    },
  };

  return {
    connection: {
      client, // tạo kết nối  đến mysql"mysql"
      ...connections[client],
      acquireConnectionTimeout: env.int("DATABASE_CONNECTION_TIMEOUT", 60000), //Knex sẽ chờ bao lâu trước khi
      //quyết định rằng không tìm thấy được kết nối và báo lỗi timeout (đơn vị tính là mili giây - milliseconds).
    },
  };
};
